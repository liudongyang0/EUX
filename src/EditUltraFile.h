#ifndef _H_EDITULTRA_FILE_
#define _H_EDITULTRA_FILE_

#include "framework.h"

int GetFileNameByOpenFileDialog( char* pcFilename, int nFilenameBufSize);
int GetFileNameBySaveFileDialog( char* pcFilename, int nFilenameBufSize);

int SplitPathFilename( char *acFullPathFilename , char *acDrivename , char *acPathFilename , char *acPathname , char *acFilename , char *acMainFilename , char *acExtFilename );

int OnNewFile( struct TabPage *pnodeTabPage );
int LoadFileDirectly( char *acFullPathFilename , FILE *fp , struct TabPage *pnodeTabPage );
int OpenFileDirectly( char *pcPathFilename );
int OpenFilesDirectly( char *acFileNames );
int OnOpenFile();
int OnCleanOpenRecentlyHistory( struct TabPage *pnodeTabPage );
int OnOpenDropFile( HDROP hDrop );
int OpenRemoteFileDirectly( struct RemoteFileServer *pstRemoteFileServer , char *acFullPathFilename );
int OnSaveFile( struct TabPage *pnodeTabPage , BOOL bSaveAs );
int OnSaveFileAs( struct TabPage *pnodeTabPage );
int OnSaveAllFiles();
int OnCloseFile( struct TabPage *pnodeTabPage );
int OnCloseAllFile();
int OnCloseAllExpectCurrentFile( struct TabPage *pnodeTabPage );

int OnEnableCreateNewFileOnNewBoot( struct TabPage *pnodeTabPage );
int OnOpenFilesThatOpenningOnExit( struct TabPage *pnodeTabPage );
int OnSetReadOnlyAfterOpenFile( struct TabPage *pnodeTabPage );

int FillAllOpenFilesOnBoot();
int CheckAllFilesSave();

int OnFileCheckUpdateWhereSelectTabPage( struct TabPage *pnodeTabPage );
int OnFilePromptWhereAutoUpdate( struct TabPage *pnodeTabPage );

int OnFileSetNewFileEols( struct TabPage *pnodeTabPage , int nNewFileEols );
int OnFileConvertEols( struct TabPage *pnodeTabPage , int nConvertEols );

int OnFileSetNewFileEncoding( struct TabPage *pnodeTabPage , int nNewFileEncoding );
int OnFileConvertEncoding( struct TabPage *pnodeTabPage , int nConvertEncoding );

int test_SplitPathFilename();

#endif
