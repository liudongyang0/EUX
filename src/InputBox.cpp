#include "InputBox.h"

#define EUINPUTBOX_CLASSNAME	"EUInputBox"

static HWND		sg_hWndParent ;
static LPCTSTR		sg_lpText ;
static LPCTSTR		sg_lpCaption ;
static UINT		sg_uType ;
static LPTSTR		sg_lpInputBuf ;
static UINT		sg_uMaxInputSize ;

static HINSTANCE	sg_hAppInstance ;
static HWND		sg_hWnd ;
static HFONT		sg_hFont ;

static HWND		sg_hStaticText ;
static HWND		sg_hEditText ;
static HWND		sg_hOkButton ;
static HWND		sg_hCancelButton ;

static int		sg_nWindowReturnValue ;

LRESULT CALLBACK InputBoxWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	WORD	uButtonId ;

	switch( message )
	{
	case WM_CREATE:
		sg_hStaticText = ::CreateWindowEx( WS_EX_RIGHT , "Static" , sg_lpText , WS_CHILD | WS_VISIBLE , 5 , 20 , 160 , 40 , hDlg , 0 , sg_hAppInstance , 0 ) ;
		if( sg_hStaticText == NULL )
			return FALSE;

		::SendMessage( sg_hStaticText , WM_SETFONT , (WPARAM)sg_hFont , 0 );

		sg_hEditText = ::CreateWindowEx( WS_EX_CLIENTEDGE , "Edit" , "" , WS_CHILD | WS_VISIBLE | ES_LEFT , 170 , 20 , 250 , 20 , hDlg , 0 , sg_hAppInstance , 0 ) ;
		if( sg_hEditText == NULL )
			return FALSE;

		::SendMessage( sg_hEditText , WM_SETFONT , (WPARAM)sg_hFont , 0 );
		if( sg_lpInputBuf[0] )
			::SendMessage( sg_hEditText , WM_SETTEXT , 0 , (LPARAM)sg_lpInputBuf );
		::SendMessage( sg_hEditText , EM_SETLIMITTEXT ,sg_uMaxInputSize , 0 ) ;

		sg_hOkButton = ::CreateWindowEx( 0 , "Button" , "ȷ��" , WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON , 290 , 60 , 60 , 20 , hDlg , (HMENU)IDOK , sg_hAppInstance , 0 ) ;
		if( sg_hOkButton == NULL )
			return FALSE;

		::SendMessage( sg_hOkButton , WM_SETFONT , (WPARAM)sg_hFont , 0 );

		sg_hCancelButton = ::CreateWindowEx( 0 , "Button" , "ȡ��" , WS_CHILD | WS_VISIBLE , 360 , 60 , 60 , 20 , hDlg , (HMENU)IDCANCEL , sg_hAppInstance , 0 ) ;
		if( sg_hCancelButton == NULL )
			return FALSE;

		::SendMessage( sg_hCancelButton , WM_SETFONT , (WPARAM)sg_hFont , 0 );

		break;
	case WM_SETFOCUS:
		::SetFocus( sg_hEditText );
		break;
	case WM_COMMAND:
		uButtonId = LOWORD(wParam) ;
		switch( uButtonId )
		{
		case IDOK:
			::GetWindowText( sg_hEditText , sg_lpInputBuf , sg_uMaxInputSize );
			sg_nWindowReturnValue = IDOK ;
			::DestroyWindow( hDlg );
			break;
		case IDCANCEL:
			sg_nWindowReturnValue = IDCANCEL ;
			::DestroyWindow( hDlg );
			break;
		}
		break;
	case WM_DESTROY:
		::PostQuitMessage(0);
		break;
	default:
		return ::DefWindowProc( hDlg , message , wParam , lParam ) ;
	}

	return TRUE;

}

int InputBox( HWND hWndParent , LPCTSTR lpText , LPCTSTR lpCaption , UINT uType , LPTSTR lpInputBuf , UINT uMaxInputSize )
{
	sg_hWndParent = hWndParent ;
	sg_lpText = lpText ;
	sg_lpCaption = lpCaption ;
	sg_uType = uType ;
	sg_lpInputBuf = lpInputBuf ;
	sg_uMaxInputSize = uMaxInputSize ;

	LOGFONT lfont;
	memset(&lfont, 0, sizeof(lfont));
	strcpy(lfont.lfFaceName, "MS Shell Dlg" );
	lfont.lfHeight = -12;
	lfont.lfWeight = FW_NORMAL;
	lfont.lfItalic = FALSE;
	lfont.lfCharSet = DEFAULT_CHARSET;
	lfont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	lfont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	lfont.lfQuality = DEFAULT_QUALITY;
	lfont.lfPitchAndFamily = DEFAULT_PITCH;
	sg_hFont = CreateFontIndirect(&lfont); 

	sg_hAppInstance = ::GetModuleHandle(NULL) ;

	WNDCLASSEX wcex;
	memset( & wcex , 0x00 , sizeof(wcex) );

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW ;
	wcex.lpfnWndProc = InputBoxWndProc ;
	wcex.cbClsExtra = 0 ;
	wcex.cbWndExtra = 0 ;
	wcex.hInstance = sg_hAppInstance ;
	wcex.hIcon = LoadIcon(0, IDI_APPLICATION) ;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW) ;
	wcex.hbrBackground = (HBRUSH)COLOR_WINDOW ;
	wcex.lpszClassName = EUINPUTBOX_CLASSNAME ;
	wcex.hIconSm = wcex.hIcon;
	::RegisterClassEx(&wcex);

	HWND hDesktop = ::GetDesktopWindow() ;
	RECT rectDesktop ;
	::GetWindowRect( hDesktop , & rectDesktop );
	sg_hWnd = ::CreateWindowEx( 0 , EUINPUTBOX_CLASSNAME , sg_lpCaption , WS_DLGFRAME | WS_VISIBLE , (rectDesktop.right-365)/2 , (rectDesktop.bottom-130)/2 , 470 , 130 , sg_hWndParent , 0 , sg_hAppInstance , 0 ) ;
	if( sg_hWnd == NULL )
		return -1;

	::EnableWindow( sg_hWndParent , FALSE ) ;
	::EnableWindow( sg_hWnd , TRUE ) ;
	::SetForegroundWindow( sg_hWnd ) ;

	::ShowWindow( sg_hWnd , SW_SHOW ) ;
	::UpdateWindow( sg_hWnd ) ;

	MSG msg ;

	sg_nWindowReturnValue = IDCANCEL ;
	while( ::GetMessage( & msg , 0 , 0 , 0 ) )
	{
		if( msg.message == WM_KEYDOWN )
		{
			if( msg.wParam == VK_RETURN )
			{
				::PostMessage( sg_hWnd , WM_COMMAND , MAKELONG(IDOK,0) , 0 );
			}
			else if( msg.wParam == VK_ESCAPE )
			{
				::PostMessage( sg_hWnd , WM_COMMAND , MAKELONG(IDCANCEL,0) , 0 );
			}
		}

		::TranslateMessage( & msg ) ;
		::DispatchMessage( & msg ) ;
	}
	
	::EnableWindow( sg_hWndParent , TRUE ) ;
	::SetForegroundWindow( sg_hWndParent ) ;

	return sg_nWindowReturnValue;
}
